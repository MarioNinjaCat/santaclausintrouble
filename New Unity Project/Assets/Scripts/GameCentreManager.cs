﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCentreManager : MonoBehaviour
{
    public GameObject santa;
    public Animator santaAnimator;
    public Rigidbody2D rb;
    public int inPause;
    public int over;
    public float speed;
    public int doubleJumple;

    public Vector2[] positions;
    public float timer;
    public float timerReset;
    public GameObject giftPrefab;
    public float time;
    public Text timeText;
    public int giftsTaken;
    public Text countText;
    public GameObject clockPrefab;
    public float timerGameOver;
    public int clockInTheScene;
    
    // Use this for initialization
    void Start()
    {
        inPause = 0;
        over = 0;
        speed = 10;
        timer = 5f;
        timerReset = 3f;
        time = 60;
        giftsTaken = 0;
        GameObject.Find("ghost").GetComponent<GhostManager>().game = 1;
        timerGameOver = 3;
    }

    void FixedUpdate()
    {
        if (over == 0)
        {
            santa.transform.Translate(speed * Input.GetAxis("Horizontal"), 0, 0);
            santa.transform.Translate(speed * Input.acceleration.x, 0, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (over == 1)
        {
            if (time <= 0)
                time = 0;

            foreach (GameObject item in GameObject.FindGameObjectsWithTag("item"))
            {
                Destroy(item);
            }
            timerGameOver -= Time.deltaTime;

        }

        if (timerGameOver <= 0)
        {

            GameObject.Find("ghost").GetComponent<GhostManager>().currentScore = giftsTaken;
            Application.LoadLevel("GameOver");

        }

        timer -= Time.deltaTime;
        if (timer <= 0 && over == 0)
        {
            if (Random.RandomRange(0, 2) != 0)
            {
                GameObject gift = Instantiate(giftPrefab, GameObject.Find("Canvas").transform);
                gift.name = "gift";
                gift.transform.localPosition = positions[Random.Range(0, positions.Length)];
                gift.transform.localPosition = new Vector2(gift.transform.localPosition.x + Random.RandomRange(-30, 30), gift.transform.localPosition.y);
                gift.transform.SetSiblingIndex(11);
            }
            else
            {
                if (clockInTheScene<=3)
                {
                    if (timerReset == 1)
                    {
                        if (Random.RandomRange(0, 5) == 0)
                        {
                            GameObject clock = Instantiate(clockPrefab, GameObject.Find("Canvas").transform);
                            clock.transform.localPosition = positions[Random.Range(0, positions.Length)];
                            clock.transform.SetSiblingIndex(11);
                            clock.name = "clock";
                            clockInTheScene++;
                        }
                    }

                    else
                    {
                        GameObject clock = Instantiate(clockPrefab, GameObject.Find("Canvas").transform);
                        clock.transform.localPosition = positions[Random.Range(0, positions.Length)];
                        clock.transform.SetSiblingIndex(11);
                        clock.name = "clock";
                        clockInTheScene++;
                    }
                }
            }

            timerReset -= 2 * Time.deltaTime;
            if (timerReset < 1f)
                timerReset = 1f;
            timer = timerReset;

        }

        if (time > 0)
            time -= Time.deltaTime;

        timeText.text = time.ToString("00.0");

        if (time <= 0)
        {
            over = 1;
            santaAnimator.SetBool("gameOver", true);

        }

        if (inPause == 0)
        {
            if ((Input.acceleration.x < 0 && Input.acceleration.x > -1) || Input.GetAxis("Horizontal") < 0)
            {

                santa.transform.localScale = new Vector3(-1, 1, 1);

            }
            if ((Input.acceleration.x > 0 && Input.acceleration.x < 1) || Input.GetAxis("Horizontal") > 0)
            {
                santa.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        //if (over == 0)
        //{
        //    santa.transform.Translate(speed * Input.acceleration.x, 0, 0);
        //    santa.transform.Translate(speed * Input.GetAxis("Horizontal"), 0, 0);
        //}

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (doubleJumple == 0)
            {
                GameObject.Find("ghost").GetComponent<GhostManager>().jump.Play();
                santaAnimator.SetBool("Jumped", true);
                rb.AddForce(220 * Vector2.up, ForceMode2D.Impulse);
            }
            if (doubleJumple == 1 &&
                //santaAnimator.GetCurrentAnimatorStateInfo(0).length > santaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime && 
                santaAnimator.GetBool("Jumped") == true)
            {
                rb.AddForce(100 * Vector2.up, ForceMode2D.Impulse);
                santaAnimator.SetBool("DoubleJump", true);
            }
            doubleJumple++;

        }

        if (santa.transform.localPosition.x < -620)
            santa.transform.localPosition = new Vector3(620, -138, this.transform.localPosition.z);
        if (santa.transform.localPosition.x > 650)
            santa.transform.localPosition = new Vector3(-590, -58, this.transform.localPosition.z);
        if (santa.transform.localPosition.y < -280)
            over = 1;
    }

    public void Jump()
    {
        if (over == 0)
        {
            if (doubleJumple == 0)
            {
                GameObject.Find("ghost").GetComponent<GhostManager>().jump.Play();
                santaAnimator.SetBool("Jumped", true);
                rb.AddForce(220 * Vector2.up, ForceMode2D.Impulse);
            }
            if (doubleJumple == 1 &&
                //santaAnimator.GetCurrentAnimatorStateInfo(0).length > santaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime && 
                santaAnimator.GetBool("Jumped") == true)
            {
                rb.AddForce(100 * Vector2.up, ForceMode2D.Impulse);
                santaAnimator.SetBool("DoubleJump", true);
            }
            doubleJumple++;
        }


    }

    public void RefreshCount()
    {
        countText.text = giftsTaken.ToString();
    }


}
