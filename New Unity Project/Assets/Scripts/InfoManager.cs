﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoManager : MonoBehaviour {
    public GameObject yourScore;

	// Use this for initialization
	void Start () {
        if (GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial == 1)
            Score();

        GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial = 0;

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void Back()
    {
        if (yourScore.active == false)
            Application.LoadLevel("MainMenu");
        else
            yourScore.SetActive(false);
    }

    public void Share()
    {
        Application.OpenURL("fb://page/2245907889027190");
    }

    public void Score()
    {
        yourScore.SetActive(true);
        GameObject.Find("recordScroll").GetComponent<Text>().text = PlayerPrefs.GetInt("score").ToString();
        GameObject.Find("recordCentre").GetComponent<Text>().text = PlayerPrefs.GetInt("scoreCentre").ToString();
    }

    public void Tutorial()
    {
        GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial = 0;
        Application.LoadLevel("Tutorial");
    }

    public void AroundTheWorld()
    {
        Application.LoadLevel("ScoreWorld");

    }
}
