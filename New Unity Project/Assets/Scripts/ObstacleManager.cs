﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public GameObject loseGifts;
    public GameObject Santa;

    // Use this for initialization
    void Start()
    {
        Santa = GameObject.Find("Santa");

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collision)
    {


            if (collision.gameObject.name == "Santa" )
            {
            GameObject.Find("ghost").GetComponent<GhostManager>().hit.Play();

            if (GameObject.Find("EventSystem").GetComponent<GameScrollManager>().giftsTaken >= 3)
                {
                    GameObject giftLoses = Instantiate(loseGifts, GameObject.Find("Canvas").transform);
                    giftLoses.name = "loseGifts";
                    transform.SetSiblingIndex(2);
                    if (Santa.transform.localScale.x == 1)
                        giftLoses.transform.localPosition = new Vector3(Santa.transform.localPosition.x - 100, Santa.transform.localPosition.y, Santa.transform.localPosition.z);
                    else
                        giftLoses.transform.localPosition = new Vector3(Santa.transform.localPosition.x + 100, Santa.transform.localPosition.y, Santa.transform.localPosition.z);


                    GameObject.Find("EventSystem").GetComponent<GameScrollManager>().giftsTaken -= 3;
                    GameObject.Find("EventSystem").GetComponent<GameScrollManager>().RefreshCount();
                    Destroy(giftLoses, 3f);
                }

                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().thereIsCube=false;

                Destroy(this.gameObject);






        }

    }
}
