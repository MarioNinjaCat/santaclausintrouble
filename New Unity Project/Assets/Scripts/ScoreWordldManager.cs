﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreWordldManager : MonoBehaviour
{
    public Animator santaAnimator;
    float timer;
    int jumped = 0;
    string leveleChosen;


    // Start is called before the first frame update
    void Start()
    {
        santaAnimator.SetInteger("chosen", -1);
        timer = 2.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (santaAnimator.GetInteger("chosen") == 3 || santaAnimator.GetInteger("chosen") == 2)
            timer -= Time.deltaTime;

        if (timer <= 0)
            Application.LoadLevel("Scores");
    }

    public void Back()
    {
        GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial = 1;
        Application.LoadLevel("Info");
    }

    public void Centre()
    {
        if (jumped == 0)
        {
            santaAnimator.SetInteger("chosen", 1);
            GameObject.Find("ghost").GetComponent<GhostManager>().scoreSelected = 1;

        }
        jumped = 1;

    }

    public void Scroll()
    {
        if (jumped == 0)
        {
            GameObject.Find("ghost").GetComponent<GhostManager>().scoreSelected = 0;
            santaAnimator.SetInteger("chosen", 0);

        }
        jumped = 1;

    }
}
