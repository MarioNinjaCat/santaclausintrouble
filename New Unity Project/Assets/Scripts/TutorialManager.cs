﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour {
    public Text description;
    public float timer;
    public Image scene;
    public Sprite[] sprites;
    public GameObject finger;
    public GameObject circle;
    public GameObject skipButton;

    // Use this for initialization
    void Start () {
        if (GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial == 0)
            skipButton.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
        if (timer >= 3f && timer <= 7f)
            description.text = "Christmas is here and it's time to bring presents!";
        if (timer >= 11f && timer <= 15f)
        {
            description.text = "...but this year you're late!";
            skipButton.SetActive(true);  
        }
        if (timer >= 19f && timer <= 23f)
            description.text = "collect all the gifts before the grinch takes them!";
        if (timer >= 27f && timer <= 31f)
        {
            description.text = "you can choose one of these games";
            circle.transform.localPosition = new Vector3(178, -5, 0);
        }
        if (timer >= 31f && timer <= 35f)
        {
            description.text = "let's see the first game";
            circle.transform.localPosition = new Vector3(-118, -5, 0);

        }
        if (timer >= 35f && timer <= 39f)
        {
            circle.transform.localPosition = new Vector3(-1000, -1000, 0);
            description.text = "run as fast as you can! ";
            scene.sprite=sprites[0];
        }
        if (timer >= 39f && timer <= 43f)
        {
            circle.transform.localPosition = new Vector3(40, 97, 0);
            description.text = "collect all the gifts that fall but pay attention to the grinch";
        }
        if (timer >= 43f && timer <= 47f)
        {
            circle.transform.localPosition = new Vector3(-295, 79, 0);
            description.text = "if the grinch collects three presents you have lost!";
        }
        if (timer >= 47f && timer <= 51f)
        {
            circle.transform.localPosition = new Vector3(-1000, -1000, 0);
            scene.sprite = sprites[1];
            description.text = "you can run faster by tilting the device or tapping on the device";
        }
        if (timer >= 51f && timer <= 55f)
        {
            description.text = "";
        }
        if (timer >= 55f && timer <= 59f)
        {
            description.text = "you can jump by swipe up!";
            finger.SetActive(true);
        }
        if (timer >= 59f && timer <= 63f)
        {
            circle.transform.localPosition = new Vector3(178, -5, 0);
            finger.SetActive(false);
            scene.sprite = sprites[2];
            description.text = "now let's see the second game";
        }
        if (timer >= 63f && timer <= 67f)
        {
            circle.transform.localPosition = new Vector3(-1000, -1000, 0);
            scene.sprite = sprites[3];
            description.text = "you have to collect as many presents in 60 seconds";
        }
        if (timer >= 67f && timer <= 71f)
        {
            scene.sprite = sprites[4];
            circle.transform.localPosition = new Vector3(-150, 33, 0);
            description.text = "you can also collect timers that will add extra seconds!";
        }
        if (timer >= 71f && timer <= 75f)
        {
            circle.transform.localPosition = new Vector3(-1000, -1000, 0);
            description.text = "Good Luck!!";
            scene.sprite = sprites[2];
        }
        if (timer >= 76f)
        {
            if(GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial == 0)
                Application.LoadLevel("Info");
            if (GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial == 1)
                Application.LoadLevel("MainMenu");
        }
    }

    public void skip()
    {
        timer = 80f;
    }
}
