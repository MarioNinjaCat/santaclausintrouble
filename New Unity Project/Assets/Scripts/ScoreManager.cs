﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text firstText;
    public Text secondText;
    public Text thirdText;
    public Text fourthText;
    public Text fifthText;
    public Text allScores;

    public GameObject panel;
    public Text Loading;
    float timer = 3f;

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("ConnectionAzure").GetComponent<EasyTables>().Scores();

    }

    // Update is called once per frame
    void Update()
    {

        if (string.IsNullOrWhiteSpace(firstText.text) && string.IsNullOrWhiteSpace(secondText.text)&& string.IsNullOrWhiteSpace(thirdText.text) && string.IsNullOrWhiteSpace(fourthText.text)&& string.IsNullOrWhiteSpace(fifthText.text))
        {
            panel.SetActive(true);
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Loading.text = "NO INTERNET CONNECTION";
                Loading.alignment = TextAnchor.MiddleCenter;
            }
            else
            {
                Loading.alignment = TextAnchor.MiddleCenter;
                timer -= Time.deltaTime;
                if (timer < 3 && timer > 2.5)
                    Loading.text = "Loading.";
                if (timer < 2.5 && timer > 2)
                    Loading.text = "Loading..";
                if (timer < 2 && timer > 1.5)
                    Loading.text = "Loading...";
                if (timer < 1)
                    timer = 3f;
            }
        }
        else
        {
            panel.SetActive(false);
            allScores.text = "CLICK HERE TO SEE ALL THE RECORDS!";
        }

    }

    public void Back()
    {
        Application.LoadLevel("ScoreWorld");
    }

    public void AllScores()
    {
        Application.OpenURL("http://santaclaussite.azurewebsites.net");
    }
}
