﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {
    float timer;
    public GameObject over;
    public GameObject score;
    public GameObject scoreText;
    public GameObject record;
    public GameObject SantaSleep;
    public GameObject zZz;
    public GameObject SantaHappy;
    int madeRecord;

    // Use this for initialization
    void Start () {
        timer = 5f;
        GameObject.Find("ghost").GetComponent<GhostManager>().grinch.Play();

        if (PlayerPrefs.GetInt("Music") == 1)
        {
            GameObject.Find("ghost").GetComponent<AudioSource>().volume = 0.1f;
        }

        madeRecord = 0;
    }
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

		if(timer<=5 && timer>4.9 && PlayerPrefs.GetInt("Music") == 1)
            GameObject.Find("ghost").GetComponent<GhostManager>().grinch.Play();

        if (timer<=0.1 && PlayerPrefs.GetInt("Music") == 1)
            GameObject.Find("ghost").GetComponent<AudioSource>().volume = 1;

        if (timer < 0)
        {
            over.SetActive(false);
            score.SetActive(true);
            scoreText.GetComponent<Text>().text = GameObject.Find("ghost").GetComponent<GhostManager>().currentScore.ToString();
            if (GameObject.Find("ghost").GetComponent<GhostManager>().game == 0)
            {            
                if (GameObject.Find("ghost").GetComponent<GhostManager>().currentScore > PlayerPrefs.GetInt("score"))
                {
                    SantaSleep.SetActive(false);
                    zZz.SetActive(false);
                    record.SetActive(true);
                    madeRecord = 1;
                }
                else
                {
                    SantaHappy.SetActive(false);
                }
            }
            if (GameObject.Find("ghost").GetComponent<GhostManager>().game == 1)
            {
                if (GameObject.Find("ghost").GetComponent<GhostManager>().currentScore > PlayerPrefs.GetInt("scoreCentre"))
                {
                    SantaSleep.SetActive(false);
                    zZz.SetActive(false);
                    record.SetActive(true);
                    madeRecord = 1;
                }
                else
                {
                    SantaHappy.SetActive(false);
                }
            }

        }

        if (timer <= -6f)
        {
            if (madeRecord == 1) { 
                if (GameObject.Find("ghost").GetComponent<GhostManager>().game == 1)  
                    PlayerPrefs.SetInt("scoreCentre", GameObject.Find("ghost").GetComponent<GhostManager>().currentScore);
                if (GameObject.Find("ghost").GetComponent<GhostManager>().game == 0)
                    PlayerPrefs.SetInt("score", GameObject.Find("ghost").GetComponent<GhostManager>().currentScore);

                GameObject.Find("ConnectionAzure").GetComponent<EasyTables>().UpdateEasyTablesTest();

            }
            Application.LoadLevel("MainMenu");

        }

    }
}
