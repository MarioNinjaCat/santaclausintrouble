﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class GameScrollManager : MonoBehaviour {
    public GameObject background;
    public GameObject terrain;
    public GameObject santa;
    public Rigidbody2D rb;
    public Animator santaAnimator;
    public int doubleJumple;
    public float speed;
    public float timer;
    public float timerReset;

    public GameObject giftPrefab;
    public int giftsTaken;
    public Text countText;
    public int giftsGrinch;
    public Text countTextGrinch;
    public int over;
    public float timerGameOver;
    public GameObject cubePrefab;
    public bool thereIsCube;
    public int inPause;

    public GameObject secondChance;
    public int dead;
    public float timerVideo;
    public GameObject seeVideo;
    public GameObject backToGameSecondChance;
    public GameObject backToGameSecondChanceText;

    // Use this for initialization
    void Start () {
        doubleJumple = 0;
        speed = 20;
        timer = 5f;
        timerReset = 3f;
        over = 0;
        timerGameOver = 3;
        thereIsCube = false;
        inPause = 0;
        GameObject.Find("ghost").GetComponent<GhostManager>().game = 0;
        dead = 0;
        timerVideo = 3f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(over==0)
        {
            santa.transform.Translate(speed * Input.GetAxis("Horizontal"), 0, 0);
            santa.transform.Translate(speed * Input.acceleration.x, 0, 0);
        }
    }

    void Update () {
        if(dead==1)
            timerVideo -= Time.deltaTime;

        if(timerVideo<=0 && dead==1)
        {
            seeVideo.SetActive(false);
            GameObject.Find("watchVideo").SetActive(false);
            backToGameSecondChance.SetActive(true);
            backToGameSecondChanceText.SetActive(true);
            dead++;
            inPause = 1;
        }

        if (dead == 1)
            seeVideo.GetComponent<Button>().interactable = false;

        if (santa.transform.localScale.x==1 && over == 0)
            background.transform.Translate(-200 * Time.deltaTime, 0, 0, Camera.main.transform) ;
        if (santa.transform.localScale.x == -1 && over == 0)
            background.transform.Translate(200 * Time.deltaTime, 0, 0, Camera.main.transform);
        //terrain.transform.Translate(-200 * Time.deltaTime, 0, 0, Camera.main.transform);
        if (over == 0)
        {
            //santa.transform.Translate(speed * Input.GetAxis("Horizontal"), 0, 0);
            //santa.transform.Translate(speed * Input.acceleration.x, 0, 0);

            if (Input.touchCount == 1)
            {
                var touch = Input.touches[0];
                if (touch.position.x < Screen.width / 2)
                {
                    rb.AddForce(new Vector2(5, 0));
                }
                else if (touch.position.x > Screen.width / 2)
                {
                    rb.AddForce(new Vector2(-5, 0));
                }
            }
        }

        if (giftsGrinch == 3)
        {
            santaAnimator.SetBool("gameOver", true);
            over = 1;
            GameObject.FindGameObjectsWithTag("Gift");
            foreach (GameObject item in GameObject.FindGameObjectsWithTag("Gift"))
            {
                Destroy(item);
            }
            
        }


        if (over == 1)
            timerGameOver-=Time.deltaTime;

        if (timerGameOver <= 0)
        {
            GameObject.Find("ghost").GetComponent<GhostManager>().currentScore = giftsTaken;
            if (dead == 0)
            {
                secondChance.SetActive(true);
                GameObject.Find("score").GetComponent<Text>().text = giftsTaken.ToString();
            }

            if (dead==3)
                Application.LoadLevel("GameOver");

        }

        if (inPause == 0)
        {
            if ((Input.acceleration.x < 0 && Input.acceleration.x > -1) || Input.GetAxis("Horizontal") < 0)
            {

                santa.transform.localScale = new Vector3(-1, 1, 1);
                santaAnimator.SetBool("back", false);

            }
            if ((Input.acceleration.x > 0 && Input.acceleration.x < 1) || Input.GetAxis("Horizontal") > 0)
            {
                santa.transform.localScale = new Vector3(1, 1, 1);
                santaAnimator.SetBool("back", false);

            }
        }
        




        if(santa.transform.localEulerAngles.z!=0)
            santa.transform.localEulerAngles = new Vector3(0f,0f,0f);
        timer -= Time.deltaTime;

        if(timer<=0 && over==0)
        {
            GameObject gift = Instantiate(giftPrefab, GameObject.Find("Canvas").transform);
            gift.name = "gift";
            gift.transform.localPosition = new Vector3(Random.Range(-520, 520), gift.transform.localPosition.y, gift.transform.localPosition.z);
            gift.transform.SetSiblingIndex(1);
            if (timerReset > 0.8)
                timerReset -= Time.deltaTime;
            timer = timerReset;

        }



        if (santa.transform.localPosition.x < -650)
            santa.transform.localPosition = new Vector3(640, santa.transform.localPosition.y, this.transform.localPosition.z);
        if (santa.transform.localPosition.x > 650)
            santa.transform.localPosition = new Vector3(-640, santa.transform.localPosition.y, this.transform.localPosition.z);

        if (background.transform.localPosition.x < -2560)
        {
            if (thereIsCube == false)
            {
                GameObject cube = Instantiate(cubePrefab, GameObject.Find("background").transform);
                cube.name = "cube";
                cube.transform.SetSiblingIndex(2);
                cube.transform.localPosition = new Vector3(Random.RandomRange(1310, 2400), cube.transform.localPosition.y, cube.transform.localPosition.z);
                thereIsCube = true;
            }
            background.transform.localPosition = new Vector3(0, 0, 0);
        }

        if (background.transform.localPosition.x > 0)
        {
            if (thereIsCube == false)
            {
                GameObject cube = Instantiate(cubePrefab, GameObject.Find("background").transform);
                cube.name = "cube";
                cube.transform.SetSiblingIndex(2);
                cube.transform.localPosition = new Vector3(Random.RandomRange(1310, 2400), cube.transform.localPosition.y, cube.transform.localPosition.z);
                thereIsCube = true;
            }
            background.transform.localPosition = new Vector3(-2560, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (doubleJumple == 0)
            {
                GameObject.Find("ghost").GetComponent<GhostManager>().jump.Play();
                santaAnimator.SetBool("Jumped", true);
                rb.AddForce(220 * Vector2.up, ForceMode2D.Impulse);
            }
            if (doubleJumple ==1 &&
                //santaAnimator.GetCurrentAnimatorStateInfo(0).length > santaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime && 
                santaAnimator.GetBool("Jumped") == true)
            {
                rb.AddForce(100 * Vector2.up, ForceMode2D.Impulse);
                santaAnimator.SetBool("DoubleJump", true);
            }
            doubleJumple++;

        }

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.position.x < Screen.width / 2)
            {
                Debug.Log("Left click");
            }
            else if (touch.position.x > Screen.width / 2)
            {
                Debug.Log("Right click");
            }
        }



    }

    public void ClickL()
    {
        rb.AddForce(20 * Vector2.left, ForceMode2D.Impulse);
    }

    public void ClickR()
    {
        rb.AddForce(20 * Vector2.right, ForceMode2D.Impulse);
    }

    public void RefreshCount()
    {
        countText.text = giftsTaken.ToString();
        countTextGrinch.text = giftsGrinch.ToString();

    }

    public void Jump()
    {
        if(over==0){
        if (doubleJumple == 0)
        {
            GameObject.Find("ghost").GetComponent<GhostManager>().jump.Play();
            santaAnimator.SetBool("Jumped", true);
            rb.AddForce(220 * Vector2.up, ForceMode2D.Impulse);
        }
        if (doubleJumple == 1 &&
            //santaAnimator.GetCurrentAnimatorStateInfo(0).length > santaAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime && 
            santaAnimator.GetBool("Jumped") == true)
        {
            rb.AddForce(100 * Vector2.up, ForceMode2D.Impulse);
            santaAnimator.SetBool("DoubleJump", true);
        }
        doubleJumple++;
        }
    }

    public void Finish()
    {
        Application.LoadLevel("GameOver");
    }

    public void Video()
    {
        if (dead == 0)
        {
            if (Advertisement.IsReady("video"))
                Advertisement.Show("video");
            dead++;
        }

    }


    public void BackToGameAfterDead()
    {

        secondChance.SetActive(false);
        giftsGrinch = 2;
        over = 0;
        timerGameOver = 3;
        inPause=0;
        timer = timerReset;
        santaAnimator.SetBool("gameOver", false);
        dead++;
        RefreshCount();
    }
}
