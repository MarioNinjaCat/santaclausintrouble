﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {
    public Sprite inPlay;
    public Sprite inPause;
    public bool IsInPause;
    public GameObject pauseMenu;
    Scene m_Scene;
    string sceneName;
    // Use this for initialization
    void Start () {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Pause()
    {
        if (IsInPause == true)
        {
            GetComponent<Image>().sprite = inPlay;
            Time.timeScale = 1;
            IsInPause = false;
            pauseMenu.SetActive(false);
            if (sceneName == "GameScroll")
                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().inPause = 0;
            if (sceneName == "GameCentre")
                GameObject.Find("EventSystem").GetComponent<GameCentreManager>().inPause = 0;

        }
        else
        {
            GetComponent<Image>().sprite = inPause;
            Time.timeScale = 0;
            IsInPause = true;
            pauseMenu.SetActive(true);
            if (sceneName == "GameScroll")
                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().inPause = 1;
            if (sceneName == "GameCentre")
                GameObject.Find("EventSystem").GetComponent<GameCentreManager>().inPause = 1;

        }
    }

    public void Home()
    {
        Time.timeScale = 1;
        Application.LoadLevel("MainMenu");
    }
}
