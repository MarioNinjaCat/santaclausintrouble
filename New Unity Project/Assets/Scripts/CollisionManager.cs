﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionManager : MonoBehaviour {
    Scene m_Scene;
    string sceneName;
    // Use this for initialization
    void Start () {
        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;

    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Santa")
        {
            if (sceneName == "GameScroll")
            {
                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().santaAnimator.SetBool("Jumped", false);
                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().doubleJumple = 0;
                GameObject.Find("EventSystem").GetComponent<GameScrollManager>().santaAnimator.SetBool("DoubleJump", false);
            }
            if (sceneName == "GameCentre")
            {
                GameObject.Find("EventSystem").GetComponent<GameCentreManager>().santaAnimator.SetBool("Jumped", false);
                GameObject.Find("EventSystem").GetComponent<GameCentreManager>().doubleJumple = 0;
                GameObject.Find("EventSystem").GetComponent<GameCentreManager>().santaAnimator.SetBool("DoubleJump", false);
            }
        }

        if (collision.gameObject.name == "SantaSelect")
        {
            try
            {
                if (GameObject.Find("EventSystem").GetComponent<SelectGameManager>().santaAnimator.GetInteger("chosen")==0)
                GameObject.Find("EventSystem").GetComponent<SelectGameManager>().santaAnimator.SetInteger("chosen", 2);
            }catch
            {
                if (GameObject.Find("EventSystem").GetComponent<ScoreWordldManager>().santaAnimator.GetInteger("chosen") == 0)
                    GameObject.Find("EventSystem").GetComponent<ScoreWordldManager>().santaAnimator.SetInteger("chosen", 2);
            }

            try
            {
                if (GameObject.Find("EventSystem").GetComponent<SelectGameManager>().santaAnimator.GetInteger("chosen") == 1)
                GameObject.Find("EventSystem").GetComponent<SelectGameManager>().santaAnimator.SetInteger("chosen", 3);
            }
            catch
            {
                if (GameObject.Find("EventSystem").GetComponent<ScoreWordldManager>().santaAnimator.GetInteger("chosen") == 1)
                    GameObject.Find("EventSystem").GetComponent<ScoreWordldManager>().santaAnimator.SetInteger("chosen", 3);
            }
        }
    }

}
