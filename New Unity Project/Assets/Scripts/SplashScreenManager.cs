﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreenManager : MonoBehaviour {

    public GameObject SantaClaus;
    public Sprite SantaFermo;
    int fineCorsa;
    float timer=3f;
    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetInt("firstTime") == 0)
        {
            GameObject.Find("ghost").GetComponent<AudioSource>().volume = 1;
            PlayerPrefs.SetInt("Music", 1);
        }


    }

    // Update is called once per frame
    void Update () {
        SantaClaus.transform.localPosition = Vector3.Lerp(SantaClaus.transform.localPosition, new Vector3(175, SantaClaus.transform.localPosition.y, SantaClaus.transform.localPosition.z), 0.002f*Time.time);
        if (SantaClaus.transform.localPosition.x >= 145 && SantaClaus.GetComponent<Image>().sprite.name == "Run (11)")
        {
            fineCorsa = 1;
            SantaClaus.GetComponent<Animator>().cullingMode = AnimatorCullingMode.CullCompletely;
            SantaClaus.GetComponent<Image>().sprite = SantaFermo;
        }

        if(fineCorsa==1)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
            if(PlayerPrefs.GetInt("firstTime")==0)
            {
                GameObject.Find("ghost").GetComponent<GhostManager>().fromTutorial = 1;
                Application.LoadLevel("CreateAccount");
            }
            else
                Application.LoadLevel("MainMenu");

        }
    }
}
