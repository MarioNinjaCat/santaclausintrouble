﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectGameManager : MonoBehaviour {
    public Animator santaAnimator;
    float timer;
    int jumped = 0;
    string leveleChosen;

	// Use this for initialization
	void Start () {
        santaAnimator.SetInteger("chosen", -1);
        timer = 2.5f;
    }

    // Update is called once per frame
    void Update () {
        if (santaAnimator.GetInteger("chosen") == 3 || santaAnimator.GetInteger("chosen") == 2)
            timer -= Time.deltaTime;

        if (timer <= 0)
            Application.LoadLevel(leveleChosen);
	}

    public void Back()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Centre()
    {
        if (jumped == 0)
        {
            santaAnimator.SetInteger("chosen", 1);
        leveleChosen = "GameCentre";

        }
        jumped = 1;

    }

    public void Scroll()
    {
        if (jumped == 0)
        {
        leveleChosen = "GameScroll";
            santaAnimator.SetInteger("chosen", 0);

        }
        jumped = 1;

    }


}
