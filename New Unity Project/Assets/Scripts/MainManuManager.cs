﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainManuManager : MonoBehaviour {
    public GameObject SantaClaus;
    bool versoDestra = true;
    public Text name;
    // Use this for initialization
    void Start () {
        name.text = PlayerPrefs.GetString("Nick").ToString();

    }
	
	// Update is called once per frame
	void Update () {

        if (SantaClaus.transform.localPosition.x >= 1100 && versoDestra==true) {
            versoDestra = false;
            
        }

        if (SantaClaus.transform.localPosition.x >= -1100 && versoDestra == true)
        {
            SantaClaus.transform.localPosition = Vector3.Lerp(SantaClaus.transform.localPosition, new Vector3(1200, SantaClaus.transform.localPosition.y, SantaClaus.transform.localPosition.z), 0.003f);
            SantaClaus.transform.localScale = new Vector3(1, 1, 1);
        }

        if (versoDestra == false)
        {
            SantaClaus.transform.localPosition = Vector3.Lerp(SantaClaus.transform.localPosition, new Vector3(-1200, SantaClaus.transform.localPosition.y, SantaClaus.transform.localPosition.z), 0.003f);
            SantaClaus.transform.localScale = new Vector3(-1, 1, 1);
        }


        if (SantaClaus.transform.localPosition.x <= -885 && versoDestra == false)
        {
            versoDestra = true;

        }


    }

    public void Settings()
    {
        Application.LoadLevel("Settings");
    }

    public void Info()
    {
        Application.LoadLevel("Info");
    }

    public void Play()
    {
        Application.LoadLevel("SelectGame");
    }
}
