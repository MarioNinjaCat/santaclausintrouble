﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostManager : MonoBehaviour
{
    public AudioSource jump;
    public AudioSource grinch;
    public AudioSource hit;
    public AudioSource click;
    public int currentScore;
    public int game;
    public int fromTutorial;
    public int fromScore;
    public int scoreSelected;
    public string player;

    // Use this for initialization
    void Start()
    {
        GameObject.Find("ghost").GetComponent<AudioSource>().volume = PlayerPrefs.GetInt("Music");
        GameObject.Find("ghost").GetComponent<GhostManager>().jump.volume = PlayerPrefs.GetInt("Sound");
        GameObject.Find("ghost").GetComponent<GhostManager>().grinch.volume = PlayerPrefs.GetInt("Sound");
        GameObject.Find("ghost").GetComponent<GhostManager>().hit.volume = PlayerPrefs.GetInt("Sound");
        GameObject.Find("ghost").GetComponent<GhostManager>().click.volume = PlayerPrefs.GetInt("Sound");



    }

    // Update is called once per frame
    void Update()
    {
        player = PlayerPrefs.GetString("Nick").ToString();

    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

}
