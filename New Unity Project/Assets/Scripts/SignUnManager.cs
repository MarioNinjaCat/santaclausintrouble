﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using Microsoft.WindowsAzure.MobileServices;

public class SignUnManager : MonoBehaviour
{
    public Button goButton;
    public Text Nickname;
    public Text Error;

    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("score",0);
        PlayerPrefs.SetInt("scoreCentre",0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Regex.IsMatch(Nickname.text, @"^[a-zA-Z0-9_]+$") )
        {
            goButton.interactable = true;
            Error.text = "";
        }
        else
        {
            goButton.interactable = false;
            Error.text = "the nickname can contain \n numbers letters and underscores";

        }
    }

    public void SignUp()
    {
        GameObject.Find("ConnectionAzure").GetComponent<EasyTables>().EasyTablesTest();
        PlayerPrefs.SetInt("firstTime", 1);
        PlayerPrefs.SetString("Nick", Nickname.text.ToString());

        Application.LoadLevel("Tutorial");

    }
}
