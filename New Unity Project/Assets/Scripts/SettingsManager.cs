﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {
    public GameObject music;
    public GameObject sound;

    public GameObject Panel;
    int ceMusica;

    // Use this for initialization
    void Start () {
        ceMusica = PlayerPrefs.GetInt("Music");

        if (PlayerPrefs.GetInt("Music") == 0)
        {
            music.GetComponent<Image>().color = new Color32(142, 142, 142, 255);
        }

        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            sound.GetComponent<Image>().color = new Color32(142, 142, 142, 255);

        }
        else
        {
            sound.GetComponent<Image>().color = new Color32(255, 255, 255, 255);

        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Back()
    {
        Application.LoadLevel("MainMenu");
    }

    public void Music()
    {
        if(PlayerPrefs.GetInt("Music") == 0)
        {
            GameObject.Find("ghost").GetComponent<AudioSource>().volume = 1;
            music.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            ceMusica = 1;
        }
        else
        {
            GameObject.Find("ghost").GetComponent<AudioSource>().volume = 0;
            music.GetComponent<Image>().color = new Color32(142, 142, 142, 255);
            ceMusica = 0;
        }

        PlayerPrefs.SetInt("Music", (int)GameObject.Find("ghost").GetComponent<AudioSource>().volume);


    }

    public void Restore()
    {
        Panel.SetActive(true);
    }

    public void Yes()
    {
        PlayerPrefs.DeleteAll();
        Panel.SetActive(false);
        SetMusic();

    }

    public void Not()
    {
        Panel.SetActive(false);
    }

    public void SetMusic()
    {

        GameObject.Find("ghost").GetComponent<AudioSource>().volume = ceMusica;
        PlayerPrefs.SetInt("Music", (int)GameObject.Find("ghost").GetComponent<AudioSource>().volume);
    }


    public void SetSound()
    {
        if (PlayerPrefs.GetInt("Sound") == 0)
        {
            GameObject.Find("ghost").GetComponent<GhostManager>().jump.volume = 1;
            GameObject.Find("ghost").GetComponent<GhostManager>().grinch.volume = 1;
            GameObject.Find("ghost").GetComponent<GhostManager>().hit.volume = 1;
            GameObject.Find("ghost").GetComponent<GhostManager>().click.volume = 1;
            sound.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        else
        {
            GameObject.Find("ghost").GetComponent<GhostManager>().jump.volume = 0;
            GameObject.Find("ghost").GetComponent<GhostManager>().grinch.volume = 0;
            GameObject.Find("ghost").GetComponent<GhostManager>().hit.volume = 0;
            GameObject.Find("ghost").GetComponent<GhostManager>().click.volume = 0;

            sound.GetComponent<Image>().color = new Color32(142, 142, 142, 255);
        }

        PlayerPrefs.SetInt("Sound", (int)GameObject.Find("ghost").GetComponent<GhostManager>().jump.volume);

    }
}
