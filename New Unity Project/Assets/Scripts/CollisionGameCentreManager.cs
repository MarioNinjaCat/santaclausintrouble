﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionGameCentreManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Santa")
        {
            GameObject.Find("EventSystem").GetComponent<GameCentreManager>().giftsTaken++;
            GameObject.Find("EventSystem").GetComponent<GameCentreManager>().RefreshCount();
            Destroy(this.gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject.Find("EventSystem").GetComponent<GameCentreManager>().time+=5;
        GameObject.Find("EventSystem").GetComponent<GameCentreManager>().clockInTheScene--;

        GameObject.Find("ghost").GetComponent<GhostManager>().click.Play();
        Destroy(this.gameObject);
    }

}
