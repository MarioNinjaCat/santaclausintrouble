﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftManager : MonoBehaviour {
    public Sprite[] gifts;
    int lastSecond;

    // Use this for initialization
    void Start () {
        GetComponent<Image>().sprite = gifts[Random.Range(0,3)];
        name = "gift";
        transform.localPosition = new Vector3(Random.Range(-520, 520), transform.localPosition.y, transform.localPosition.z);
        transform.SetSiblingIndex(1);
        GetComponent<Rigidbody2D>().gravityScale = Random.RandomRange(5, 10);
        transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(-5,5));

    }

    // Update is called once per frame
    void Update () {
        if (transform.localPosition.y < -320)
            Destroy(gameObject);

        if (this.gameObject.GetComponent<Animation>().isPlaying==false && lastSecond == 1)
        {
            GameObject.Find("EventSystem").GetComponent<GameScrollManager>().giftsGrinch++;
            Destroy(gameObject);
            GameObject.Find("EventSystem").GetComponent<GameScrollManager>().RefreshCount();
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Santa")
        {
            GameObject.Find("EventSystem").GetComponent<GameScrollManager>().giftsTaken++;
            GameObject.Find("EventSystem").GetComponent<GameScrollManager>().RefreshCount();
            Destroy(gameObject);
        }

        if (collision.gameObject.tag == "terrain" && lastSecond==0)
        {
            this.gameObject.GetComponent<Animation>().Play();
            lastSecond = 1;
        }


    }
}
