﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure.MobileServices;
using UnityEngine;
using UnityEngine.UI;

public class EasyTables : BaseMobileApps
{
	public async void EasyTablesTest()
	{
		//ClearOutput();
		//WriteLine("-- Testing Easy Tables --");

		//WriteLine("Getting table");
		IMobileServiceTable<TodoItem> tbl = Client.GetTable<TodoItem>();

		//WriteLine("Inserting new item");
		try
		{
            TodoItem record = new TodoItem();
            record.Id = GameObject.Find("Text").GetComponent<Text>().text;
            record.pointsScroll = PlayerPrefs.GetInt("score");
            record.pointsCentre = PlayerPrefs.GetInt("scoreCentre");
            await tbl.InsertAsync(record);
            //await tbl.InsertAsync(new TodoItem { Text = "New item" });

            //WriteLine("Getting unfinished items");
            //List<TodoItem> list = await tbl.Where(i => i.Complete == false).ToListAsync();
            //foreach(TodoItem item in list)
            //	WriteLine($"{item.Id} - {item.Text} - {item.Complete}");

            //WriteLine("Updating first item");
            //list[0].Complete = true;
            //await tbl.UpdateAsync(list[0]);

            //WriteLine("Deleting first item");
            //await tbl.DeleteAsync(list[0]);
            PlayerPrefs.SetInt("firstTime", 1);
            PlayerPrefs.SetString("Nick", GameObject.Find("Text").GetComponent<Text>().text);
        }
        catch (Exception e)
		{
			//WriteLine(e.ToString());
		}

		//WriteLine("-- Test Complete --");
	}

    public async void UpdateEasyTablesTest()
    {
        //ClearOutput();
        //WriteLine("-- Testing Easy Tables --");

        //WriteLine("Getting table");
        IMobileServiceTable<TodoItem> tbl = Client.GetTable<TodoItem>();

        //WriteLine("Inserting new item");
        try
        {
            //TodoItem record = new TodoItem();
            //record.Id = PlayerPrefs.GetString("Nick").ToString();
            //record.pointsScroll = PlayerPrefs.GetInt("score");
            //record.pointsCentre = PlayerPrefs.GetInt("scoreCentre");
            //await tbl.UpdateAsync(record);

            //await tbl.InsertAsync(new TodoItem { Text = "New item" });

            //WriteLine("Getting unfinished items");
            List<TodoItem> list = await tbl.Where(i => i.Id == PlayerPrefs.GetString("Nick").ToString()).ToListAsync();
            list[0].pointsScroll = PlayerPrefs.GetInt("score");
            list[0].pointsCentre = PlayerPrefs.GetInt("scoreCentre");
            await tbl.UpdateAsync(list[0]);

            //WriteLine("Deleting first item");
            //await tbl.DeleteAsync(list[0]);
        }
        catch (Exception e)
        {
            //WriteLine(e.ToString());
        }

        //WriteLine("-- Test Complete --");
    }

    public async void Scores()
    {
        
        IMobileServiceTable<TodoItem> tbl = Client.GetTable<TodoItem>();
        try
        {
            List<TodoItem> list = await tbl.ToListAsync();
            List<TodoItem> CentreSortedList = list.OrderByDescending(o => o.pointsCentre).ToList();
            List<TodoItem> ScrollSortedList = list.OrderByDescending(o => o.pointsScroll).ToList();

            if (GameObject.Find("ghost").GetComponent<GhostManager>().scoreSelected ==0)
            {
                GameObject.Find("firstText").GetComponent<Text>().text = ScrollSortedList[0].Id.ToString() +" "+ ScrollSortedList[0].pointsScroll.ToString();
                GameObject.Find("secondText").GetComponent<Text>().text = ScrollSortedList[1].Id.ToString() + " " + ScrollSortedList[1].pointsScroll.ToString();
                GameObject.Find("thirdText").GetComponent<Text>().text = ScrollSortedList[2].Id.ToString() + " " + ScrollSortedList[2].pointsScroll.ToString();
                GameObject.Find("fourthText").GetComponent<Text>().text = ScrollSortedList[3].Id.ToString() + " " + ScrollSortedList[3].pointsScroll.ToString();
                GameObject.Find("fifthText").GetComponent<Text>().text = ScrollSortedList[4].Id.ToString() + " " + ScrollSortedList[4].pointsScroll.ToString();
            }
            if (GameObject.Find("ghost").GetComponent<GhostManager>().scoreSelected == 1)
            {
                GameObject.Find("firstText").GetComponent<Text>().text = CentreSortedList[0].Id.ToString() + " " + CentreSortedList[0].pointsCentre.ToString();
                GameObject.Find("secondText").GetComponent<Text>().text = CentreSortedList[1].Id.ToString() + " " + CentreSortedList[1].pointsCentre.ToString();
                GameObject.Find("thirdText").GetComponent<Text>().text = CentreSortedList[2].Id.ToString() + " " + CentreSortedList[2].pointsCentre.ToString();
                GameObject.Find("fourthText").GetComponent<Text>().text = CentreSortedList[3].Id.ToString() + " " + CentreSortedList[3].pointsCentre.ToString();
                GameObject.Find("fifthText").GetComponent<Text>().text = CentreSortedList[4].Id.ToString() + " " + CentreSortedList[4].pointsCentre.ToString();
            }

        }
        catch (Exception e)
        {
            //WriteLine(e.ToString());
        }

        //WriteLine("-- Test Complete --");
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
